<?php
namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
class LoginController extends Controller
{  
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function show_login_form()
    {
        return view('auth.login');
    }
    public function process_login(Request $request)
    {   
        //$request->request->add(['role' => '0']);
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $credentials = $request->except(['_token']);
        $user = User::where('email',$request->email)->first();
        if (auth()->attempt($credentials)) {
            return redirect()->route('dashboard');
        }else{ 
            session()->flash('message', 'Invalid credentials. Please try again.');
            return redirect()->back();
        }
    }
    public function show_signup_form()
    {
        return view('auth.register');
    }
    public function process_signup(Request $request)
    {   
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);
 
        $user = User::create([
            'name' => trim($request->input('name')),
            'email' => strtolower($request->input('email')),
            'password' => bcrypt($request->input('password')),
        ]);

        session()->flash('message', 'Your account is created');
       
        return redirect()->route('login');
    }
    public function logout()
    {
        \Auth::logout();
        return redirect()->route('login');
    }
    public function show_forgot_form()
    {
        return view('auth.forgot');
    }
    public function process_forgot(Request $request)
    {   
        $request->validate([
            'email' =>'required|email|exists:users',
            'password' => 'required',
            'c_password' => 'required_with:password|same:password',
        ],
        ['email.exists' => 'You enter email dose not exits']);
        $updated = User::where([
                            'email' => $request->email])
                ->update(['password' => bcrypt($request->input('password'))]);
        if($updated) {
            session()->flash('succ_message', 'Password hase been change successfully');
            return redirect()->route('login');
        }else {
            session()->flash('message', 'Something went wrong try with correct email..!');
       
        return redirect()->route('forgot');
        }
        
        
    }

}
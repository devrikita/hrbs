<?php

namespace App\Http\Controllers;
use App\Models\User; 
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Hash;
use App\Traits\UploadTrait;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Mail;
use DB;
use URL;
use App\Traits\ProfilePictureTrait;
use Illuminate\Support\Facades\Log;
use App\Imports\UsersImport;
use App\Jobs\UserCreateJob;
use App\Mail\NewusercreateEmail;


class UsersController extends Controller
{
    use UploadTrait;
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {   
        $h_title = 'Members';
        $breadcrumb = [['title'=> "Members",'url'=>'#'],['title'=> "All Members",'url'=> route('member.index')]];
        $isEmpty = User::where('role','1')->count(); 
        if($isEmpty == 0){
            return view('member.elist', compact('h_title'));
        }else{
            if ($request->ajax()) {
            $result =User::where('role','1')->latest()->get(); 
            return  Datatables::of($result) 
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                            $edit = route("member.edit",$row->id);
                            $delete = "delete_rec('".$row->id."')";
                            $btn = '<div class="dropdown"><button type="button" class="tr_option ic-action dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item" href="'.$edit.'"><span class="ic-edit"></span> Edit</a>
                            <a onclick="'.$delete.'"  href="javascript:void(0);" class="delete_btn dropdown-item" href="#"><span class="ic-trash"></span> Delete</a>
                            </div>
                            </div>';
                           
                            return $btn;
                        })
                    
                        ->addColumn('profile',function($row){
                            if(file_exists(public_path().$row->profile) && !empty($row->profile)){
                                $img = URL::asset($row->profile);
                            }else { 
                                $img = URL::asset('/img/no-user.png');
                            }
                            return  '<img src="'.$img.'" class="img_width">';
                        })
                        ->rawColumns(['action','profile'])
                        ->make(true);
            } 
            return view('member.list', compact('h_title','breadcrumb'));
        }
        
    }
    public function show($id=0){ 
       
        $title = ($id !='')?'Edit':'Add';
        $breadcrumb = [
            ['title'=> "Members",'url'=> route('member.index')],
            ['title'=> $title,'url'=> '#']
        ];   
        if($id !=''){
            $id = $id;
            $data = User::where('id',$id)->latest()->first(); 
            return view('member.add', compact('data','breadcrumb'));
        }
        return view('member.add', compact('breadcrumb'));
    }
    public function save(Request $request){  
       
        $breadcrumb = [
            ['title'=> "Members",'url'=> route('member.index')],
            ['title'=> "Members",'url'=> '#']
        ];
        $this->Validation($request);
        $id = $request->id;
        if($id != ''){
            $update_data = array(
                'name' => $request->name,
                'last_name' => $request->last_name,
                'email'=>$request->email,
                'contact_no'=>$request->contact_no
            );
            if ($request->has('profile')) {  
                $image = $request->file('profile');
                $name = time();
                $folder = '/uploads/users/';
                $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
                $fileupload_name = $this->uploadOne($image, $folder, 'public', $name); 
                if($fileupload_name != false){
                    if($request->prev_photo !=''){ 
                        $oldfilename =  substr(strrchr($request->prev_photo, '/'), 1); 
                        $this->deleteOne($folder,$oldfilename); 
                    }
                    $update_data['profile'] =$filePath; //prd($update_data);
                }
            } 
            User::where('id', $id)->update($update_data);
            return redirect('member/list')->with('success',str_replace( '{mname}','Member',UPDATE_SUC));
        }else{ 
            $apidetails = DB::table('settings')->whereIn('slug', ['app','ios'])->get();
            $app ='';
            $ios = '';
            if(!empty($apidetails)){
            foreach($apidetails as $urls){
                if($urls->slug == 'app'){
                    $app = $urls->value;
                }
                if($urls->slug == 'ios'){
                    $ios = $urls->value;
                }
            }}
            $password = random_string('alnum',8);
            $insert_data = array(
                'name' => $request->name,
                'last_name' => $request->last_name,
                'email'=>$request->email,
                'contact_no'=>$request->contact_no,
                'password'=>Hash::make($password)
            );
            if ($request->has('profile')) {  
                $image = $request->file('profile');
                $name = time();
                $folder = '/uploads/users/';
                $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
                $fileupload_name = $this->uploadOne($image, $folder, 'public', $name); 
                if($fileupload_name != false){
                    if($request->prev_photo !=''){ 
                        $oldfilename =  substr(strrchr($request->prev_photo, '/'), 1); 
                        $this->deleteOne($folder,$oldfilename); 
                    }
                    $insert_data['profile'] =$filePath; 
                }
            } 
           
            if($user = User::create($insert_data)){
                $details = [
                    'name' => $request->name.' '.$request->last_name,
                    'email'=>$request->email,
                    'password'=>$password,
                    'appUrl' => $app,
                    "iosUrl" => $ios
                ];
                Log::info("build");
                //Mail::send($request->email,new NewusercreateEmail($details));
                Mail::to($request->email)->send(new NewusercreateEmail($details));
                //$this->dispatch(new UserCreateJob($user, $password));
                return redirect('member/list')->with('success',str_replace( '{mname}','Member',INSERT_SUC));
            }
        }
    }
    public function Validation($request){
        $id = $request->id;
        $rules = ['name' => 'required',
        'email'=>'required|unique:users,email,'.$id,
        'last_name'=>'required',
        'profile' =>'max:10000']; 
        $messages = [
            'profile.max' => 'Upload may not be greater than 10 MB.',
        ];
       
        return $this->validate($request,$rules,$messages);
    }
    public function statusUpdate(Request $request)
    {   
        $update_data = array(
            'status' => $request->value,
        );
        $id = $request->pk;
        User::where('id', '=', $id)->update($update_data);
        $result = array(
            "status" => 'success',
        );
        echo json_encode($result);die;
    }
    public function deleteRecord(Request $request){
        $id = $request->id;
        User::where('id', '=', $id)->delete();
        $result = array(
            "status" => 'success',
            'message'=>str_replace( '{mname}','Member',MULDELETE_SUC)
        );
        echo json_encode($result);die;
    }    
    public function actionRecord(Request $request){
        //print_r($request->all());exit;
        $ids = $request->chk;
        if($request->action =='active'){
            foreach($ids as $id)
			{					
                User::where('id', '=', $id)->update(array('status'=>'active'));
			}
			$res=array('status'=>'success','message'=>MULACTIVE_SUC);
			echo json_encode($res);die;
        }
        if($request->action =='inactive'){
            foreach($ids as $id)
			{							
                User::where('id', '=', $id)->update(array('status'=>'inactive'));
			}
			$res=array('status'=>'success','message'=>MULINACTIVE_SUC);
			echo json_encode($res);die;

        }
        if($request->action =='delete'){
            foreach($ids as $id)
			{							
                User::where('id', '=', $id)->update(array('is_delete'=>'1'));
			}
			$res=array('status'=>'success','message'=>MULDELETE_SUC);
			echo json_encode($res);die;
        }
        
    }
    public function uploadFile(Request $request){
        Excel::import(new UsersImport,request()->file('file'));  
        return back()->with('success','Members excel import successfully');
    }
}

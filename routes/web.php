<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::namespace('App\Http\Controllers\Auth')->group(function () {
    Route::get('/','LoginController@show_login_form');
    Route::get('/login','LoginController@show_login_form')->name('login');
    Route::post('/login','LoginController@process_login')->name('login');
    Route::get('/forgot','LoginController@show_forgot_form')->name('forgot');
    Route::post('/forgot','LoginController@process_forgot')->name('forgot');
    Route::get('/register','LoginController@show_signup_form')->name('register');
    Route::post('/register','LoginController@process_signup');
    Route::get('/logout','LoginController@logout')->name('logout');
  });
Route::get('/dashboard', 'App\Http\Controllers\HomeController@index')->name('dashboard');
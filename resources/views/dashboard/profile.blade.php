@extends('layouts.master')
@section('title', 'Add App User')
@section('headerpageasset')
@endSection 
@section('content')
@include('layouts.breadcrumb')

</div>
<!-- breadcrumb - close -->
@if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session('success'))
                <p class="alert alert-success"> {{session('success')}} <p>
            @endif 

    <form action="{{route('update.profile')}}" enctype="multipart/form-data" method="post"  id="frm_submit1"  class="form-horizontal form-material">
    {{ csrf_field() }}   
    
<div class="row">
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <input type="hidden" value="{{$data->id}}" name="id">
        <div class="card"> 
            <div class="card-body">
                <center>
            <?php if (auth()->user()->photo){?>
                <img id="photo_prev" src="{{ URL::asset(auth()->user()->photo) }}"  width="150">
            <?php }else { ?>
                <img id="photo_prev" src=""  width="150">
            <?php } ?>
            </center>
                <input type="file" class="form-control-file m-t-15" name="photo" id="photo" aria-describedby="fileHelp" accept="image/jpeg, image/png, image/jpg" method="post">
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-xlg-9 col-md-7">
        <div class="card">
            <div class="tab-pane" id="settings" role="tabpanel">
                <div class="card-body">
                    <div class="form-group">
                        <label class="col-md-12">Full Name <span class="error">*</span></label>
                        <div class="col-md-12">
                            <input type="text" value="{{$data->name}}" placeholder="Johnathan Doe" class="form-control" name="name" id="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="example-email" class="col-md-12">Email <span class="error">*</span></label>
                        <div class="col-md-12">
                            <input type="email" readonly value="{{$data->email}}" placeholder="Email" class="form-control" name="email" id="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Password</label>
                        <div class="col-md-12">
                            <input type="password" value="" name="password" id="password" class="form-control form-control-line">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Confirm Password</label>
                        <div class="col-md-12">
                            <input type="password" value="" name="cpassword" id="cpassword" class="form-control form-control-line">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="submit" class="btn btn-success" value="Update Profile" />
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    </form>
</div>
@endSection
@section('footerpageasset')
<script src ="{{URL::asset('developer/js/baradmin/profile.js') }} "type="text/javascript"></script>
@endSection
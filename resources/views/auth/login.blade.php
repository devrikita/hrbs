@extends('layouts.withoutlogin')
@section('content')
<style>
    label.error {
    color: red;
    font-size: 12px;
    font-weight: 500 !important;
}</style>
<div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in </p>
      @if (session('message'))
            
            <div class="alert alert-warning">
            {{ session('message') }}
            </div>
             @endif
             @if (session('succ_message'))
            
            <div class="alert alert-success">
            {{ session('succ_message') }}
            </div>
          
            @endif
        <form id="form" name="form" method="post" action="{{ route('login') }}">
        @csrf
    
        <div class="input-group mb-2">
          <input type="email" name="email" id="email" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          
        </div>
        <label id="email-error" class="error" for="email" style="display:block"></label>
        <div class="input-group mb-2">
          <input type="password" name="password" id="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>    
        </div>
        <label id="password-error" class="error" for="password" style="display:none"></label>
        <div class="row">
          <div class="col-4">
            <div class="icheck-primary">
              
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4 text-center">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      
      <hr>
      <!-- /.social-auth-links -->
        <div class="row">
          <div class="col-12">
                <p class="mb-1">
                    <a href="{{ route('forgot') }}">Forgot password?</a>
                </p>
            </div>
        </div>
        <div class="row">
          <div class="col-12">
                <p class="mb-1">
                <a href="{{route('register')}}" class="text-center">Register a new membership</a>
                </p>
            </div>
        </div>
      
      
    </div>
    <!-- /.login-card-body -->
  </div>

    <script>
    $(document).ready(function () { 
        $('#form').validate({ 
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    
                },
            }
                                
        });
    });
    </script>




@endSection
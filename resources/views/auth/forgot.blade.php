@extends('layouts.withoutlogin')
@section('content')
<style>
    label.error {
    color: red;
    font-size: 12px;
    font-weight: 500 !important;
}</style>
<div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">You forgot your password? Here you can easily retrieve a new password. </p>
      @if (session('message'))
            
            <div class="alert alert-warning">
            {{ session('message') }}
            </div>
          
        @endif
        @foreach($errors->all() as $error)
        <div class="alert alert-warning">{!!   $error !!}</div>
                @endforeach
            <form id="form" name="form" method="post" action="{{ route('forgot') }}">
            @csrf
    
        <div class="input-group mb-2">
          <input type="email" name="email" id="email" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          
        </div>
        <label id="email-error" class="error" for="email" style="display:block"></label>
        <div class="input-group mb-2">
          <input type="password" name="password" id="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>    
        </div>
        <label id="password-error" class="error" for="password" style="display:none"></label>
        <div class="input-group mb-2">
          <input type="password" name="c_password" id="c_password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>    
        </div>
        <label id="password-error" class="error" for="c_password" style="display:none"></label>
        <div class="row">
          <div class="col-4">
            <div class="icheck-primary">
              
            </div>
          </div>
          <!-- /.col -->
          <div class="col-12 text-center">
            <button type="submit" class="btn btn-primary btn-block">Confirm password</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      
      <hr>
      <!-- /.social-auth-links -->
        <div class="row">
          <div class="col-12">
                <p class="mb-1">
                    <a href="{{ route('login') }}">Login</a>
                </p>
            </div>
        </div>
        <div class="row">
          <div class="col-12">
                <p class="mb-1">
                <a href="{{route('register')}}" class="text-center">Register a new membership</a>
                </p>
            </div>
        </div>
      
      
    </div>
    <!-- /.login-card-body -->
  </div>
       
           
              
        
        <script>
        $(document).ready(function () { 
            $.validator.addMethod("checklower", function(value) {
            return /[a-z]/.test(value);
            });
            $.validator.addMethod("checkupper", function(value) {
            return /[A-Z]/.test(value);
            });
            $.validator.addMethod("checkdigit", function(value) {
            return /[0-9]/.test(value);
            });
            $.validator.addMethod("pwcheck", function(value) {
            return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) && /[a-z]/.test(value) && /\d/.test(value) && /[A-Z]/.test(value);
            });
            $('#form').validate({ 
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        //minlength: 6,
                        maxlength: 30,
                        required: true,
                        //checklower: true,
                        //checkupper: true,
                        //checkdigit: true
                    },
                    c_password: {
                        required: true,
                        equalTo: "#password",
                    },
                },
                messages: {
                password: {
                pwcheck: "Password is not strong enough",
                checklower: "Need atleast 1 lowercase alphabet",
                checkupper: "Need atleast 1 uppercase alphabet",
                checkdigit: "Need atleast 1 digit"
                }
            },
                                  
            });
        });
        </script>

@endSection

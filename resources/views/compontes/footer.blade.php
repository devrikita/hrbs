<!-- Footer Starts --> 
<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date('Y');?> <a href="javascript:void(0)"> Developer</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block"></div>
  </footer>
<!-- Footer Ends -->
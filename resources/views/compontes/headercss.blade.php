<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="csrf-token" content="{{ csrf_token() }}"> 
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>{{ config('app.name') }} @if(View::hasSection('title')) @yield('title')  @else  Admin @endif</title>
  <!-- Tell the browser to be responsive to screen width -->
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon.png') }}">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  {{--<link rel="stylesheet" href="{{asset('plugins/jqvmap/jqvmap.min.css')}}">--}}
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.min.css')}}">

<link rel="stylesheet" href="{{asset('css/style.css')}}"> <!-- main -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
@if(View::hasSection('headerpageasset'))
  @yield('headerpageasset')
@endif
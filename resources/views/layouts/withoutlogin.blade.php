<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="csrf-token" content="{{ csrf_token() }}"> 
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>{{ config('app.name') }} @if(View::hasSection('title')) @yield('title')  @else  Login @endif</title>
  <!-- Tell the browser to be responsive to screen width -->
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon.png') }}">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
 
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
<!-- icheck bootstrap -->
<link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('css/style.css')}}">
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js') }}"></script>
<script src="{{asset('js/additional-methods.min.js') }}"></script>
</head>


<body class="login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="{{ route('login') }}"><b>HRBS</b></a>
    </div>
    @yield('content')
  </div>
  

  <!-- Bootstrap 4 -->
  <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <!-- thm App -->
  <script src="{{asset('js/adminlte.min.js')}}"></script> 
 



</body>
</html>

<!DOCTYPE html>
<html>
<head>
  @include('compontes.headercss')
</head>


<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="{{asset('img/AdminLTELogo.png')}}" alt="loading..." height="60" width="60">
  </div>
  @include('compontes.navbar')
  @include('compontes.sidebar')
  
  <div class="content-wrapper">
    
    @yield('content')
  </div>
  
  @include('compontes.footer')  
  @include('compontes.footerjs')
  <!--Extra Page JS -->
</div>
</body>
</html>

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFiledToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('status')->default(0)->comment('0-for inactive,1-active');
            $table->string('role','100')->default('staff')->comment('admin,staff');
            $table->string('profile_img')->default(null);
            $table->string('phone_no')->default(null);
            $table->text('address')->default(null);
            
        });
    }
}
